import numpy as np
import laspy

def load_data(filepath: str):
    """点群データを読み込む関数

        引数:
            filepath (str): ファイルの保管ディレクトリを指定(自動)

        Returns:
            tuple: (points, colors)
        """
    # lasファイルを展開
    las_file = laspy.file.File(filepath, mode='r')

    # 点群の座標情報を取得
    points = np.vstack((las_file.x, las_file.y, las_file.z)).T
    print(points)

    # 点群のRGB情報を取得
    colors = np.vstack((las_file.red / 65535.0, las_file.green / 65535.0, las_file.blue / 65535.0)).T

    # lasファイルを閉じる
    las_file.close()

    # サンプリングする点の数を計算
    num_points = len(points)
    print(num_points)

    return points, colors



def load_label(filepath: str):
    return np.load(filepath).astype(np.uint16)


def save_label(filepath: str, labels):
    with open(filepath, 'wb') as f:
        np.save(f, labels.astype(np.uint16))
