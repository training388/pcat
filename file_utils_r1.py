import numpy as np
import laspy
import os
import open3d as o3d

def load_data(filepath: str):
    """
    点群データを読み込む関数 (PLY / LAS形式対応)

    引数:
        filepath (str): ファイルパス

    Returns:
        tuple: (points, colors)
    """
    file_extension = filepath.split('.')[-1].lower()

    if file_extension == 'ply':
        # PLYファイルを読み込む
        pcd = o3d.io.read_point_cloud(filepath)
        points = np.asarray(pcd.points)
        colors = np.asarray(pcd.colors) if pcd.has_colors() else None

    elif file_extension == 'las':
        # LASファイルを読み込む
        las_file = laspy.file.File(filepath, mode='r')  # laspy.file.File を使用
        points = np.vstack((las_file.x, las_file.y, las_file.z)).T
        colors = None
        print(las_file.red)
        if hasattr(las_file, 'red') and hasattr(las_file, 'green') and hasattr(las_file, 'blue'):
            colors = np.vstack((
                las_file.red / 65535.0,
                las_file.green / 65535.0,
                las_file.blue / 65535.0
            )).T
        las_file.close()  # リソースを解放

    else:
        raise ValueError(f"Unsupported file format: {file_extension}")

    # 点の数を出力（デバッグ用）
    print(f"Loaded {len(points)} points from {filepath}")
    print(f"Points shape: {points.shape}")
    print(f"Colors shape: {colors.shape}" if colors is not None else "No colors provided")

    return points, colors

def load_label(filepath: str):
    return np.load(filepath).astype(np.uint16)


def save_label(filepath: str, labels):
    with open(filepath, 'wb') as f:
        np.save(f, labels.astype(np.uint16))

def save_data(filepath, points, colors=None):
    """
    点群データとその色を保存する関数

    引数:
        filepath (str): 保存先のファイルパス
        points (numpy.ndarray): 点群の座標データ (N, 3)
        colors (numpy.ndarray): 点群の色情報 (N, 3)は0~1で正規化
    """

    # ファイル拡張子を取得
    file_extension = os.path.splitext(filepath)[1].lower()

    try:
        if file_extension == ".ply":
            # PLY形式で保存
            point_cloud = o3d.geometry.PointCloud()
            point_cloud.points = o3d.utility.Vector3dVector(points)
            if colors is not None:
                point_cloud.colors = o3d.utility.Vector3dVector(colors)
            o3d.io.write_point_cloud(filepath, point_cloud)
            print(f"PLY形式でデータを保存しました: {filepath}")

        elif file_extension == ".las":
            # LAS形式で保存 (laspy 1.5.0対応)
            header = laspy.header.Header(point_format=2)
            header.scale = [0.001, 0.001, 0.001]  # 適切なスケールを設定
            header.offset = [points[:, 0].min(), points[:, 1].min(), points[:, 2].min()]  # 点群の最小値をオフセットに設定

            las_file = laspy.file.File(filepath, mode="w", header=header)

            # 座標データを保存
            las_file.x = points[:, 0]
            las_file.y = points[:, 1]
            las_file.z = points[:, 2]

            # 色データを保存
            if colors is not None:
                las_file.red = (colors[:, 0] * 65535).astype(np.uint16)
                las_file.green = (colors[:, 1] * 65535).astype(np.uint16)
                las_file.blue = (colors[:, 2] * 65535).astype(np.uint16)

            las_file.close()  # ファイルをクローズ
            print(f"LAS形式でデータが保存されました: {filepath}")


        else:
            raise ValueError(f"対応していないファイル形式です: {file_extension}")

    except Exception as e:
        raise IOError(f"データの保存に失敗しました: {e}")

