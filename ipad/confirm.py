import numpy as np
import open3d as o3d

# ファイルパスを設定
ply_file = "data/gaishi.ply"
bin_file = "data/gaishi_labels.bin"

# PLYファイルを読み込む
def load_ply(file_path):
    pcd = o3d.io.read_point_cloud(file_path)  # Open3Dで点群を読み込む
    points = np.asarray(pcd.points)  # 点群の座標を取得
    return points

# BINファイルを読み込む
def load_bin(file_path):
    labels = np.fromfile(file_path, dtype=np.int16)  # BINファイルを読み込む
    return labels

# データを読み込む
points = load_ply(ply_file)
labels = load_bin(bin_file)

# PLYとBINのデータ数を確認
print(f"Number of points in PLY file: {len(points)}")
print(f"Number of labels in BIN file: {len(labels)}")

# データ数が一致しているかチェック
if len(points) != len(labels):
    print("\nError: The number of points and labels does not match!")
    exit(1)

# 各点の座標とラベルを表示
print("\nFirst 10 points and their corresponding labels:")
for i in range(min(10, len(points))):  # 最初の10個を表示
    print(f"Point {i}: {points[i]} -> Label: {labels[i]}")

# 全点の座標とラベルを保存する例（オプション）
output_file = "points_and_labels.txt"
with open(output_file, "w") as f:
    for i in range(len(points)):
        f.write(f"Point {i}: {points[i][0]}, {points[i][1]}, {points[i][2]} -> Label: {labels[i]}\n")

print(f"\nAll points and labels have been saved to '{output_file}'.")
