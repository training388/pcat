import numpy as np
import laspy

import open3d as o3d
import numpy as np

def load_data(filepath: str):
    """点群データを読み込む関数

    引数:
        filepath (str): ファイルの保管ディレクトリを指定(自動)

    Returns:
        tuple: (points, colors)
    """
    # plyファイルを読み込む
    pcd = o3d.io.read_point_cloud(filepath)
    
    # 点群の座標情報を取得 (numpy配列に変換)
    points = np.asarray(pcd.points)
    print(points)

    # 点群の色情報を取得 (numpy配列に変換)
    colors = np.asarray(pcd.colors) if pcd.has_colors() else None

    # サンプリングする点の数を計算
    num_points = len(points)
    print(num_points)

    return points, colors

def load_label(filepath: str):
    return np.load(filepath).astype(np.uint16)


def save_label(filepath: str, labels):
    with open(filepath, 'wb') as f:
        np.save(f, labels.astype(np.uint16))
