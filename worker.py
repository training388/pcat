import traceback, sys
from PyQt5.QtCore import QRunnable, QObject, pyqtSlot, pyqtSignal


# ref: https://www.pythonguis.com/tutorials/multithreading-pyqt-applications-qthreadpool/
class WorkerSignals(QObject):
    '''
    バックグラウンドタスクの状態や処理結果をsignalとして返すクラス

    finished
        signalにデータは含まず、終了したことのみを発信するsignal

    error
        バックグラウンドタスクで異常が発生した際に(例外処理として)発信するsignal
        tuple (exctype, value, traceback.format_exc() )
        exctype: 発生した異常の型
        value: 詳細な異常のメッセージ
        traceback.format_exc():tracebackのstack情報

    result
        バックグラウンドタスクの結果を返すsignal(データは任意の型で対応可能)

    progress
        処理の進捗状況を示すsignal(整数値)
        int indicating % progress

    '''
    finished = pyqtSignal()
    error = pyqtSignal(tuple)
    result = pyqtSignal(object)
    progress = pyqtSignal(int)


class Worker(QRunnable):
    '''
    GUI更新を行うメインのthreadとは別に実行する処理(GUIアプリで長時間継続して実行が必要な処理)を追加するためのクラス

    QRunnableを継承してworker threadの状態管理を実施

    :param callback: argsとkwargsをrunner関数に渡すためのcallback関数
    :param args: callback関数に渡す引数(tuple)
    :param kwargs: callback関数に渡すキーワード引数(dict)

    '''

    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()

        # Store constructor arguments (re-used for processing)
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSignals()

        # Add the callback to our kwargs
        # self.kwargs['progress_callback'] = self.signals.progress

    @pyqtSlot()
    def run(self):
        '''
        Workerがバックグラウンドタスクを実行するための開始メソッド
        '''

        try:
            # 渡された関数self.fnを引数（self.args, self.kwargs）とともに実行。
            result = self.fn(*self.args, **self.kwargs)
        except:
            # 例外を検知してerrorに関するsignalを発信
            traceback.print_exc()
            exctype, value = sys.exc_info()[:2]
            self.signals.error.emit((exctype, value, traceback.format_exc()))
        else:
            # 処理が正常に完了した際の結果を返す
            self.signals.result.emit(result)  # Return the result of the processing
        finally:
            # バックグラウンドタスクの終了
            self.signals.finished.emit()  # Done
